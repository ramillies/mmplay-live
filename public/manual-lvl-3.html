<!doctype html>
<html>
	<head>
		<title>Semivivum &mdash; manual</title>
		<meta charset="utf-8">
		<link href="style.css" rel="stylesheet">
	</head>
	<body class=grid>
		<header id=pageHeader>
			<h2>Semivivum installation manual</h2>
		</header>

		<nav id=mainNav>
			<ul>
				<li><a href="manual.html">Overview</a></li>
				<li><a href="manual-usb-boot.html">A primer on booting from USB</a></li>
				<li><a href="manual-lvl-1.html">Level 1 installation</a></li>
				<li><a href="manual-lvl-2.html">Level 2 installation</a></li>
				<li><b>Level 3 installation</b></li>
				<li><a href="manual-usage.html">Using the system</a></li>
				<hr>
				<li><a href="manual-faq.html">FAQ/Troubleshoot</a></li>
				<li><a href="manual-configs.html">Example configurations</a></li>
				<hr>
				<li><a href="index.html">Back to the main page</a></li>
			</ul>
		</nav>
		
		<article id=mainArticle>
			<h3>Level 3</h3>

			<p>This is the installation procedure that offers the most persistence, and, sadly, the most hardships along the way. Once you install it, however, you will have a completely self-contained flash drive with as much persistence as you can fit on it. If you have a 32 GB flash drive, you can probably carry around your whole collection of old games, along with all your save files and settings, and play them at nearly any computer you ever encounter.</p>

			<p>To do it, you will need <b>2 flash drives</b> (and 2 USB ports for them):</p>
			<ul>
				<li>One of them will contain the system and your games &mdash; I will call it the <i>target drive</i>. <b>This drive will be formatted and all data on it will be destroyed.</b> Make sure that you don't need anything on it. Ideally, this flash drive should be decent (USB 3 helps), with capacity of 16 GB or more. (2 GB would be sufficient, but this method would be an overkill for that.) Everything that's not used for the system itself (which takes 850 MB) will be used for persistence, so with a 16 GB USB stick, you can use roughly 15 GB for your games. <b>The flash drive won't be usable with Windows any more,</b> until you format it anew (and get rid of all of your progress with semivivum).</li>
				<li>The other flash drive can be pretty much anything with at least 850 MB of free space. I will call it the <i>helper drive</i>. It won't be formatted and the data won't be destroyed, and this drive will be needed only during the installation. The target drive will be completely independent of it.</li>

			<hr>

			<h4>The Procedure</h4>

			<p><b>1.</b> Grab the <i>helper drive</i> and perform the <a href="manual-lvl-1.html">level 1 installation</a> on it.</p>

			<p><b>2.</b> <a href="manual-usb-boot.html">Boot the system from it.</a> <b>The system has its own local copy of this manual, accessible from the desktop.</b> Therefore, there is no need to memorize or print out this manual page.</p>

			<p><b>3.</b> You will be presented with a desktop. (Have a look at the <a href="manual-usage.html#overview">overview of the desktop</a> if anything isn't clear.) Plug in the <i>target drive</i> (but leave the helper drive in!)</p>

			<p><b>NOTE:</b> Resist the urge to configure the system to suit your fancy now! The persistence is still not set up, so anything you change will be forgotten on reboot anyway. After you finish the steps of this level and reboot, you can configure to your heart's content.</p>

			<p><b>4.</b> Click on the "Applications" button in the top left corner. Choose System → GParted.

			<hr>

			<h4>Formatting the target drive using GParted</h4>

			<p><b>WARNING!</b> Please perform the following steps with utmost care! This is an easy and fast way to destroy your data if you do something wrong!</p>

			<p><b>NOTE:</b> We've made the <i>helper drive</i> just to be able to boot from it and use GParted to partition the <i>target drive</i> in a certain manner. So this section of the procedure is actually the whole point of the complicated approach.</p>

			<p><b>5.1.</b> When GParted starts, you will be shown a window like this:</p>

			<img src="screenshots/gparted/1-overview.png">

			<p><b>NOTE:</b> I took the images on my system, so what you will actually see will be a bit different. The difference is only in styling, colors and so on, though. Everything still works the same way and is in the same places as I show here.</p>

			<p><b>5.2.</b> Use the drop-down list (marked with the red border on the previous image) to choose your <i>target drive</i>. You will probably see three things in the drop-down list: "/dev/sda" (your main hard drive), "/dev/sdb" (the <i>helper drive</i>) and "/dev/sdc" (the <i>target drive</i>), but they may be labeled a bit differently, so always double-check.</p>

			<p><b>5.3.</b> Did you double-check what the window shows? Either way, do it (again). Your flash drive will probably contain a single FAT32 partition (marked in bright green). If there are multiple partitions, some of them NTFS, you're most probably viewing your main hard drive. If any of the partitions show a lock icon, you're most probably viewing the <i>helper drive</i>. If you think that you're viewing the <i>target drive</i>, check that the size fits (however, it will be somewhat less than you think, e. g. the screenshot shows a view of my flash drive that's supposed to be 8 GB).</p>

			<p><b>5.5.</b> If you're absolutely sure that you're viewing the <i>target drive</i>, click all the partitions in the main list and press the Delete key on your keyboard. You should end up with this:</p>

			<img src="screenshots/gparted/2-empty.png">

			<p><b>5.5.</b> Now there are no partitions and everything is unallocated. Hit the Insert key on your keyboard to create a new partition. You'll get something like this:</p>

			<img src="screenshots/gparted/3-newpart.png">

			<p><b>5.6.</b> Fill it in just as it is shown on the image &mdash; set the "new size" to <b>850 MiB</b> and the file system to ext2 (it's ext4 by default, so be sure to change it). I used the label "base", but you can use any label you want (even an empty one, but I would advise you to fill in something). When you're satisfied, hit the Add button.</p>

			<img src="screenshots/gparted/4-partial.png">

			<p><b>5.7.</b> Select the rest of the unallocated space and hit Insert once more to make another partition:</p>

			<img src="screenshots/gparted/5-persistence.png">

			<p><b>5.8.</b> Change the file system to ext2 as well and use the label "persistence" (no quotes). <b>Make sure it's exactly the word <i>persistence</i>, otherwise your persistence won't work!</b> GParted offers to use up all the remaining space by default, which is probably what you want. Hit "Add" once more.</p>

			<img src="screenshots/gparted/6-result.png">

			<p><b>5.9.</b> Up until now, the actions have been just queued (you can see them on the bottom). If you're content with everything, then press the button with the green checkmark in the top menu (there's the word "Apply" under it on this image, but you won't see the word on your system). <b>Up until now, you can back away. If you hit the green checkmark button, you commit the changes and all data on the target drive will be lost.</b></p>

			<p><b>5.10.</b> It will take a minute or two to format the <i>target drive</i>. When it is done, right-click the first, 850 MiB partition (which I've labeled "base"), and choose "Manage Flags", like this:

			<img src="screenshots/gparted/7-flags.png">

			<p><b>5.11</b> A window with different flags pops up. Choose the "boot" flag:</p>

			<img src="screenshots/gparted/8-boot.png">

			<p><b>5.12.</b> If everything went well, you should end up with this:</p>

			<img src="screenshots/gparted/9-end.png">

			<p>If not, you can delete the partitions and make new ones &mdash; anything that has been on the drive previously is now destroyed anyway, so you can meddle with it to your heart's content.</p>

			<p><b>5.13.</b> When you're finished, you can close GParted.</p>

			<hr>

			<h4>Continuing the system installation</h4>

			<p><b>6.</b> Now you should see two new icons on the desktop, corresponding to the newly created partitions (so, if you did everything exactly like me, they will have labels "base" and "persistence"). Double-click the base one to open it in the file explorer. (It has to be mounted in order to perform the installation.)</p>

			<p><b>7.</b> Use the file explorer to locate the Semivivum image <a href="manual-usage.html#hdd">on the hard drive of your computer</a>. When you find the image, select it and press Ctrl+C. (You can do it in the window that popped up when you double-clicked the base partition icon.)</p>

			<p><b>8.</b> Now, you can actually install semivivum on the <i>target drive</i>, too. Click on the "Applications" menu once more, and choose System → Unetbootin. You're already familiar with this program, so you can perform the installation just in the same way as before. Choose the "Disk Image" option and paste the image path into the text box by using Ctrl+V in it. Don't forget to choose the correct device in the "Device" box at the bottom (most likely, it will be "/dev/sdc1" &mdash; you can find this identifier in GParted, as shown on the last image). </p>

			<p><b>9.</b> While the installation goes on, double-click the "persistence" icon on your desktop. An empty file explorer window pops up. Right-click the empty space in the explorer and choose "Open Terminal Here". (The path will be different than the one shown in the image; don't let it throw you off.)</p>

			<img src="screenshots/linux/5-persist-open-term.png">

			<p><b>10.</b> A terminal pops up and awaits your commands. We will need to put a configuration file into the root of the partition (otherwise the persistence wouldn't work). A proper configuration file is already prepared in your home directory. Hence, write this command:</p>
			<pre>sudo mv ~/persistence.conf .</pre>
			<p>exactly as you see it here, and press Enter:</p>

			<img src="screenshots/linux/6-term-persistence-conf.png">

			<p style="font-size:80%"><b>Explanation:</b> <code>mv</code> is the command for moving files. This command moves the file <code>~/persistence.conf</code> (the ~ is a shortcut that means your home directory) into the directory you're currently in (that's the dot). The command <code>sudo</code> gives one command absolute power (the command is executed as if you were <code>root</code>, the super-user). We need it because the partitions on removable media are generally mounted as read-only for everyone except the root. (If you tried to copy the file with the file manager, you would get a "Permission denied" popup.)</p>

			<p><b>11.</b> Reboot the machine. From now on, everything you change in the system will be stored into the persistence file. It's time to <a href="manual-usage.html">use the system</a>. If you want, you can delete the Semivivum from the helper drive (just delete the files). Now you will use only the target drive ever after.</p>

		</article>

		<footer id=pageFooter>
			<p style="margin: 0px">Written by Ramillies, 2019. E-mail address: ramillies at email dot cz. Page style is pretty much copied from <a href="https://rufus.ie">the webpages of Rufus tool</a>. Hosted at <a href="https://gitlab.com">GitLab</a>.</p>
			<p style="margin: 0px">This website is completely static. It uses no Javascript, no cookies etc. In other words, I don't spy on you.</p>
		</footer>

	</body>
</html>
